"""
Question/Answering system front

Author: William Arias

app v: 1.0

"""

import streamlit as st
from transformers import pipeline
import json
import os


def read_context():
    local_path = os.environ['ARTIFACT']
    with open(local_path, 'r') as output_data:
        json_data = json.load(output_data)
        return json_data['context']


@st.cache(suppress_st_warning=True)
def answer_question(question, context):
    """Runs question-answering pipeline on the input text"""
    reader = pipeline('question-answering')
    answer = reader(question=question, context=context)
    return answer


@st.cache(suppress_st_warning=True)
def run_sentiment(text):
    """Runs sentiment analysis pipeline on the input text"""
    classifier = pipeline('sentiment-analysis')
    sentiment = classifier(text)
    return sentiment


def main():
    """Executes all functions and renders streamlit UI Functions"""
    st.header("Ask us about GitLab")
    st.subheader("Type your question in the box below:")
    text = st.text_area('', 'What is GitLab?' )
    #st.write(run_sentiment(text))
    context = read_context()
    st.write(answer_question(text, context))


if __name__ == "__main__":
    main()
