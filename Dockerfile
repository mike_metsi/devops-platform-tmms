FROM python:3.8-slim-buster
COPY ./app.py /app/app.py
COPY  ./requirements.txt /app/requirements.txt
COPY ./src/1_data_cleaning/data/processed_data/context_file.json /app/context_file.json
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT ["streamlit", "run"]
CMD ["app.py"]
